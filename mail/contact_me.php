<?php
if(isset($_POST['email'])) {

// EDIT THE BELOW TWO LINES AS REQUIRED
$email_to = "developer@cnstudentdashboard.com";
$email_subject = "You Got Mail!";

function errorMesg() {
    // Error code can go here
    echo "We are very sorry, but there were error(s) found with the form you submitted. ";
    echo "<br /><br />";
    echo "Please go back and fix these errors.<br /><br />";
    die();
}

// validation expected data exists
if(!isset($_POST['name']) ||
    !isset($_POST['email']) ||
    !isset($_POST['phone']) ||
    !isset($_POST['message'])) {
    errorMesg();       
}

$name = $_POST['name']; // required
$email_address = $_POST['email']; // required
$phone = $_POST['phone']; // required
$message = $_POST['message']; // required

$email_message = "Form details from cnstudentdashboard.com as below.\n\n";

function clean_string($string) {
  $bad = array("content-type","bcc:","to:","cc:","href");
  return str_replace($bad,"",$string);
}

$email_message .= "Name: ".clean_string($name)."\n\n";

$email_message .= "Email: ".clean_string($email_address)."\n\n";

$email_message .= "Phone: ".clean_string($phone)."\n\n";

$email_message .= "Message: ".clean_string($message)."\n";

// create email headers

$headers = ‘From: ‘.$email_address.”\r\n”.
‘Reply-To: ‘.$email_from.”\r\n” .
‘X-Mailer: PHP/’ . phpversion();

/* Prepare autoresponder subject */

$respond_subject = "Thank you for contacting us!";

/* Prepare autoresponder message */

$respond_message = "Thank you for contacting Your Organisation!

We will get back to you as soon as possible.

If you would like to sign up to our newsletter please click the link below.

http://www.website.com/newsletter2.html

Yours sincerely,

Your Name

Your Organisation

www.website.com/

";
/* Send the response message using mail() function */

mail($email_address, $respond_subject, $respond_message);

/* Send the message using mail() function */

mail($email_to, $email_subject, $email_message, $headers);

//redirect to the 'thank you' page
header('Location: ./contact-form-thank-you.html');
?>

